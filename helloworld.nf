#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "hello_world"

include {
    job;
} from './nxf_modules/workflow'

workflow {
    job(
        channel.fromPath(params.inputs),
        params.function,
        params.job_uuid
    )
}