#!/usr/bin/env nextflow
nextflow.enable.dsl=2

import groovy.json.JsonOutput

params.publishDir           = null
params.label                = ''
params.scratch              = false
params.cpus                 = null
params.memory               = null
params.envfile              = null
params.absWorkdir           = ''

// SLURM only
if (params.envfile){
    params.containerOptions = params.envfile
} else {
    params.containerOptions = ''
}

/**
 * Write JobRequestFunction parameters as as JSON file 
 * so it can be consumed by the prepareInputs process.
 */
process createParamsJson {
    input:
    val jobReqFunc

    output:
    val jobReqFunc.function,        emit: function_name
    val jobReqFunc.uuid,            emit: uuid
    path 'params.json',             emit: params_json

    """
    echo '${JsonOutput.prettyPrint(JsonOutput.toJson(jobReqFunc.parameters))}' > params.json
    """
}

/**
 * Create the input dir which contains all downloaded platform data organised by uuid.
*/
process createInputdir {
    output:
    path 'input/*', emit: inputdir, type: 'any'

    """
    mkdir -p input && touch input/empty
    """
}

process prepareInputs {
    tag "$job_uuid"
    label 'auth'

    containerOptions params.containerOptions

    // local testing
    if (params.publishDir){
        publishDir params.publishDir, mode: 'copy'
    }

    input:
    val func_name
    val job_uuid
    path func_params
    path inputdir,  stageAs: 'input/*'

    output:
    path 'input_params.json',   emit: params
    path 'input/*',             emit: inputdir,  type: 'any', includeInputs: true
    path 'script/*',            emit: scriptdir, type: 'any'
    val job_uuid,               emit: uuid

    script:
    """
    if [ -n "$params.absWorkdir" ]; then
        mkdir -p input script && touch input/empty script/empty
    fi

    python /srv/app/manage.py runscript prepare_inputs --script-args function=$func_name params=$func_params jobuuid=$job_uuid
    # enable me when lib updated
    # workdir=$params.absWorkdir
    """
}

process prepareResultAsInput {
    debug true

    input:
    val job_uuid
    path results,  stageAs: 'input/tmp/results.zip'
    path jobinfo,  stageAs: 'input/tmp/jobinfo.json'
    
    output:
    path 'input/*', emit: inputdir, type: 'any', optional: true

    """
    #!/usr/bin/env bash
    mv input/tmp/ input/${job_uuid}/
    unzip input/${job_uuid}/results.zip
    """
}


process runFunction {
    tag "$job_uuid"

    scratch = params.scratch
    cpus    = params.cpus
    memory  = params.memory
    label   = params.label

    containerOptions params.containerOptions

    // local testing
    if (params.publishDir){
        publishDir params.publishDir, mode: 'copy'
    }

    input:
    path func_params
    path inputdir,          stageAs: 'input/*'
    path scriptdir,         stageAs: 'script/*'
    val job_uuid
    
    output:
    path 'output/*',        emit: outputdir
    path 'results.zip',     emit: results_zip
    path 'jobinfo.json',    emit: jobinfo
    val  job_uuid

    script:
    """
    if [ -n "$params.absWorkdir" ]; then
        mkdir -p output && touch output/empty
    fi

    export TASK_CPUS=${task.cpus}
    python /srv/app/manage.py runscript run_func --script-args params=$func_params scriptdir=script cpus=${task.cpus}
    """
}

process uploadResult {
    tag "$job_uuid"
    label 'auth'
    
    containerOptions params.containerOptions

    input:
    path 'output/*'
    path results_zip
    path jobinfo
    val job_uuid

    """
    python /srv/app/manage.py runscript upload --script-args uuid=$job_uuid result=$results_zip jobinfo=$jobinfo
    """
}
