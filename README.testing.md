# Workflow Functions - Deployment

Automated tests for Functions should be implemented directly in the Gitlab pipeline.  They execute via Nextflow on platform Gitlab Runners and perform an end to end validation of Function execution against a supplied parameter json file.

Pipelined Function tests should:

- Be based on real world use cases, but scaled back as required in order to run quickly with limited resources. 
- Peform regression testing against known outputs (Geotiff and CSV comparison tools available).

