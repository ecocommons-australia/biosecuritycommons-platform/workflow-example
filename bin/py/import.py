#!/usr/bin/env python3
""" 
Deploy Schema and Workflow Template artifacts and test/example data to the platform.

This is intended to be used as part of a deployment pipeline. 

Expects the following environment:
    
    KEYCLOAK_SERVER_URL
    KEYCLOAK_REALM
    KEYCLOAK_CLIENT_ID
    KEYCLOAK_CLIENT_SECRET_KEY

    CI_SERVER_PROTOCOL
    CI_SERVER_HOST
    CI_PROJECT_PATH

    DATA_INGESTER_API_URL
    FUNCTION_MANAGER_API_SCHEMA_URL
    WORKFLOW_MANAGER_API_TEMPLATE_URL
"""

import os
import os.path
import glob
import base64
import json
import logging
import sys
from urllib.parse import quote
from pathlib import Path
from func_utils.core.authsession import AuthSession

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

log = logging.getLogger('Import')

if os.getenv('SOURCE_REPO_URL'):
    SOURCE_REPO_URL = os.getenv('SOURCE_REPO_URL')
else:
    # Determine from Gitlab CI vars
    SOURCE_REPO_URL = (os.getenv('CI_SERVER_PROTOCOL')
                       + '://'
                       + os.getenv('CI_SERVER_HOST')
                       + '/'
                       + os.getenv('CI_PROJECT_PATH')
                       + '/')

FUNCTION_MANAGER_API_SCHEMA_URL = os.getenv('FUNCTION_MANAGER_API_SCHEMA_URL')
WORKFLOW_MANAGER_API_TEMPLATE_URL = os.getenv('WORKFLOW_MANAGER_API_TEMPLATE_URL')

DATA_INGESTER_API_URL = os.getenv('DATA_INGESTER_API_URL')
DATA_INGESTER_API_IMPORT_ENDPOINT = "importInternal/"
DATA_INGESTER_API_DELETE_ENDPOINT = "api/dataset/{UUID}/delete"


class ImportException(Exception):
    pass


def usage():
    usage_str = (f"Usage: import.py TYPE PATH [VERSION] [COMMIT_REF]\n"
                 f"\n"
                 f" import.py data      data.json\n"
                 f" import.py template  template.json  0.1\n"
                 f" import.py schema    schema.json    0.1   develop\n")
    print(usage_str)


def run(args):
    try:
        if args[1] is None or args[2] is None:
            raise Exception("Missing parameters")
    except Exception:
        usage()
        sys.exit(1)

    import_type = args[1]
    import_path = args[2]
    version = None
    ref = None
    try:
        version = args[3]
    except IndexError:
        version = os.getenv('CI_COMMIT_SHA')
    try:
        ref = args[4]
    except IndexError:
        ref = os.getenv('CI_COMMIT_SHA')

    overwrite = os.getenv('IMPORT_OVERWRITE', '').lower() in ("true", "1")

    try:
        log.info(f"Import.py {import_type} {import_path} {version} {ref}")

        session = AuthSession()

        def _import(file_path):
            if import_type == 'template':
                return import_template(session, file_path, version)
            if import_type == 'schema':
                return import_schema(session, file_path, version, ref)
            if import_type == 'data':
                return import_data(session, file_path, overwrite)
            log.error(f"Unknown Import type ({import_type})")

        for ipath in import_path.splitlines():
            if os.path.isdir(ipath):
                for filename in os.listdir(ipath):
                    f = os.path.join(ipath, filename)
                    if os.path.isfile(f):
                        _import(f)
            else:
                _import(ipath)

        sys.exit(0)

    except ImportException as e:
        log.error(f"Error ({str(type(e).__name__ )}) {str(e)}")
        sys.exit(1)


def import_template(session: AuthSession, import_path: dict, version: str):
    """ 
    Import Workflow Templates to the Workflow API.
    """
    if WORKFLOW_MANAGER_API_TEMPLATE_URL is None:
        raise ImportException('WORKFLOW_MANAGER_API_TEMPLATE_URL not exported')

    log.info(f"Importing {import_path}")
    tmpl = json.load(open(import_path))
    payload = {
        **tmpl,
        'version': version
    }
    if 'components' in tmpl:
        payload['components'] = json.dumps(tmpl.get('components'))

    log.debug(payload)

    resp = session.post(
        WORKFLOW_MANAGER_API_TEMPLATE_URL,
        json=payload
    )
    log.info("Response: " + str(resp.content))

    if resp.status_code not in (200, 409):
        raise ImportException(f"Failed to import Template: {str(resp.status_code)}")


def import_schema(session: AuthSession, schema_path: str, version: str, ref: str):
    """ 
    Import Function schemas to the Function Manager API
    """
    if FUNCTION_MANAGER_API_SCHEMA_URL is None:
        raise ImportException('FUNCTION_MANAGER_API_SCHEMA_URL not exported')
    if os.getenv('CI_COMMIT_SHA') is None:
        raise ImportException('CI_COMMIT_SHA not exported')
    if os.getenv('CI_PROJECT_PATH') is None:
        raise ImportException('CI_PROJECT_PATH not exported')

    log.info(f"import_schema: {schema_path}")

    try:
        schema = json.load(open(schema_path))

        if not isinstance(schema, dict):
            raise ImportException(f"Could not parse '{schema_path}'")

        # Nextflow pipeline url.
        # This uses a repo naming convention.
        # It is expected to match schema name ie: bsdesign.json -> bsdesign.nf
        # Only specify url if the schema describes a top level Function interface (input, output).
        if (isinstance(schema.get('input'), dict) and isinstance(schema.get('output'), dict)):
            pipeline = os.path.join(SOURCE_REPO_URL, Path(os.path.basename(schema_path)).stem + '.nf')
        else:
            pipeline = ''

        schemaid = schema.get('$id', schema.get('input', {}).get('$id'))
        if not isinstance(schemaid, str):
            raise ImportException(f"Schema does not define input.$id'")

        api_url = FUNCTION_MANAGER_API_SCHEMA_URL

        payload = {
            'name':           schema.get('title', schema.get('name')),
            'description':    schema.get('description'),
            'schemaid':       schemaid,
            'pipeline':       pipeline,
            'mdpath':         schema_path,
            'version':        version,
            'hash':           ref,
            'projectid':      os.getenv('CI_PROJECT_PATH')
        }

        resp = session.get(api_url+'?schemaid='+quote(schemaid, safe=''))
        if resp.status_code != 200:
            raise ImportException(f"Failed to import: {str(resp.status_code)}")

        if (len(resp.json()) > 0):
            # update
            uuid = resp.json()[0].get('uuid')
            resp = session.post(api_url+uuid+'/action', json=payload)
            if resp.status_code != 200 and resp.status_code != 409:
                raise ImportException(f"Failed to import: {str(resp.status_code)} ({str(resp.content)})")
        else:
            # register
            resp = session.post(api_url, json=payload)
            if resp.status_code != 200:
                raise ImportException(f"Failed to import: {str(resp.status_code)} ({str(resp.content)})")

    except Exception as e:
        raise ImportException(f"Failed to import: {e}") from e


def load_json(fpath: str) -> dict:
    with open(fpath, "r", encoding="utf-8") as f:
        return json.load(f)


def read_file_base64(fpath: str, encoding="utf-8") -> str:
    with open(fpath, "rb") as f:
        return base64.b64encode(f.read()).decode(encoding)


def import_data(session: AuthSession, import_json_path: str, overwrite=False):
    """ Import test data """
    if DATA_INGESTER_API_URL is None:
        raise ImportException('DATA_INGESTER_API_URL not exported')

    for item in load_json(import_json_path):
        log.debug(f"Processing import file ({item})")

        if item.get('delete') is True or overwrite is True:
            session.delete(
                DATA_INGESTER_API_URL + 'api/dataset/' + item.get('uuid') + '/delete?force=true'
            )

        if 'uploadedFiles' in item:
            payload = {
                **item,
                'uploadedFiles': [{
                    'name': os.path.basename(item['uploadedFiles'][0]),
                    'data': read_file_base64(item['uploadedFiles'][0])
                }]
            }
            url = DATA_INGESTER_API_URL + DATA_INGESTER_API_IMPORT_ENDPOINT

            resp = session.post(url, json=payload)

            log.info(f"Response: ({url}) {str(resp.content)}")
            if resp.status_code != 200 and resp.status_code != 409:
                raise ImportException(f"Failed to import Dataset: {str(resp.status_code)}")


if __name__ == "__main__":
    run(sys.argv)
