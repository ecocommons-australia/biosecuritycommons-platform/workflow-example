import os
import logging
from requests_oauthlib import OAuth2Session
from oauthlib.oauth2 import BackendApplicationClient

log = logging.getLogger('AuthSession')

class AuthSession:
    def __init__(self):
        self.client_id = os.getenv('KEYCLOAK_CLIENT_ID')
        self.token_endpoint = os.path.join(
            os.getenv('KEYCLOAK_SERVER_URL'),
            'realms',
            os.getenv('KEYCLOAK_REALM'),
            'protocol/openid-connect/token'
        )
        client = BackendApplicationClient(client_id=self.client_id)
        self.session = OAuth2Session(client=client)

    def fetch_token(self):
        return self.session.fetch_token(
            token_url=self.token_endpoint,
            client_id=self.client_id,
            client_secret=os.getenv('KEYCLOAK_CLIENT_SECRET_KEY')
        )

    def get(self, url):
        # get request method
        self.fetch_token()
        resp = self.session.get(url)
        log.info(f"Response: ({url}) {str(resp.content)}")
        return resp

    def post(self, url, json):
        # post request method
        self.fetch_token()
        resp = self.session.post(url, json=json)
        log.info(f"Response: ({url}) {str(resp.content)}")
        return resp
