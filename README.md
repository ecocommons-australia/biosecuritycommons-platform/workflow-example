# Workflow Functions - Example

[README.authoring.md](./README.authoring.md)

[README.localdev.md](./README.localdev.md)

[README.deployment.md](./README.deployment.md)

[README.testing.md](./README.testing.md)

Workflow Functions example repository.

Contains a boilerplate structure for implementing a new Workflow. It provides examples and stubs.
This is based on the Biosecurity Commons `bsc-functions` repo, which is itself based on the Ecocommons `sdm-function` repo.


## What is included
- Boilerplate for implementing a new Function, including schema, R integration and platform integrated Nextflow pipeline.
- Boilerplate for implementing a 'Workflow Template' to drive the frontend via the Workflow Manager API.
- Development documentation


## Getting started

### Repository setup
- Clone this repository or create a new one and copy elements as required.
- Ensure repository visibility is set to **public**.
- Ensure public pipelines are **disabled** (Settings > CI/CD > General pipeline).
- Create a protected branch named `develop`.
- Review `.gitlab-ci` (recommend in Gitlab pipeline editor) and ensure needed env vars are present and correct.


## Nextflow pipelines
![Nextflow pipeline](/assets/img/pipeline.png)

- Functions are executed via a Nextflow pipeline https://nextflow.io/. These are defined in the repository root. Typically as one pipeline per Function although other configurations are possible.
- The `dsl2` folder contains prototype pipeline abstractions that are under development.


## Docker images
- All pipeline jobs are executed via Docker containers.  These are defined within the Function repository and built via Gitlab pipeline.
- Typically, the image that contains the scientific payload is divided into a 'base' image, that installs dependencies, and an executing image that contains the platform integration scripts.  

This is because R dependencies can grow very large and have build times that run into several hours.  Seperating out these dependencies allows for rapid iteration of platform code.

### Shared code with `sdm-function`
`Dockerfile-utils` is based on `sdm-function/utils`.
- This should eventually be refactored out of sdm-function and moved to it's own repo.


## Function scripts and schemas
![Nextflow pipeline](/assets/img/function.png)
See [README.authoring.md](./README.authoring.md)


## Workflow Templates
See [README.authoring.md](./README.authoring.md)


## Known issues and limitations
- Schemas and toolkit param config files duplicate the Function `outputs`, with the pconfig being the current source of truth. This needs to be fixed (with the schema becoming the source of truth).
- Schemas with complex branching and external references cannot be fully introspected when compiling the Function metadata.


