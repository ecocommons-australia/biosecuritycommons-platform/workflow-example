# Workflow Functions - Dev and debugging

## Prereading
This guide assumes basic familarity with Docker, Docker Compose, command line (bash) and Nextflow.

It is written primarily for MacOS and Linux but should be reproducable using Windows WSL.

## Running a pipeline and R scripts locally

Local development has the following minimum install requirements:
- Nextflow 
- Docker
- Environment file with platform information (`.env`)

### Install Nextflow

Nextflow requires an updated version of Java.
Java installed on a Mac by default is probably not going to work. 
The best way to install new versions of Java is with [SdkMan](https://sdkman.io/install).

#### Install Java 17.0.11 (Temurin) with SdkMan

```shell

sdk install java 17.0.11-tem
# Open a new shell for this to take effect
```

```shell
# Nextflow downloads to the folder the install script is run from.
curl -s https://get.nextflow.io | bash
# Copy nextflow to a $PATH location to make it a generally available command.
cp ./nextflow /usr/local/bin
```

### Environment and Authorisation
Executing pipelines generally involves talking to the platform. As such credentials are required.

An env file `.env.example` configured for Dev environment is available, copy this to `.env` to activate it.

This does not contain any actual credentials, these come from your personal account.

#### Login to the platform

```shell
./bin/login.sh
```  

This will open a browser window with the standard login.

When successfully logged in, a file `.env.auth` will be created containing an access token. 
This will automatically be used when running a pipeline via the below commands (until it expires).


### Run pipeline
The script `./bin/run_pipeline.sh` is designed to run a pipeline using locally built docker images. 

The exact script arguments are provided by running with no arguments.

Images must first be built with the command.

```shell
docker compose build
```

---
Check docker-compose.yml and docker-compose.override.example.yml for build and run options.
---

A pipeline can then be run locally to reproduce execution similar to how it occurs in the platform.

`run_pipeline.sh`  will run any pipeline in the repo eg. bsspread.nf’ with any param.json file.  This will perform all the steps the platform would in terms of pulling the data based on UUIDs, setting up the folder structure all locally.  It will execute all steps except for the final `upload_result` step which may fail.

```shell
./bin/run_pipeline.sh ./bsspread.nf ./src/docs/examples/bsspread-example.json
```

Or alternatively:

```shell
./bin/run_job.sh ./helloworld.nf JOB_UUID
```

Artefacts created by the Nextflow job:
```
./nxf_work/input/
./nxf_work/output/
./nxf_work/scripts/
```

### Debuging R scripts locally with platform data.

`run_rscript.sh` is a short cut to ‘re run’ just the R script.  This only works if `run_pipeline.sh` has been executed atleast once before hand. It will run the R script in the nextflow work dir `bin/work/script/script.R`.

he integration script can be tweaked and rerun, or the docker images rebuilt (see `docker-compose.yml`).  Just be sure to copy any changes made to the workdir script back to the original master.

```shell
./bin/run_rscript.sh SCRIPT_NAME
```

This allows for basic debugging but deeper introspection and dev should be done using the integrated RStudio environment.

### Debuging prepare_inputs only

`run_prepare_inputs.sh` executes the prepare_inputs.py script directly and can be used for debugging this process. 

```shell
./bin/run_prepare_inputs.sh FUNC_NAME PARAMS (container relative) [any additional args for prepare_inputs.py]
```

## RStudio integration 

`Dockerfile-rstudio` can be optionally built for local R development directly integrated with a RStudio web IDE.

Requires the following dependency to be unzipped and placed in the repo root under `rocker_scripts/`

https://griffitheduau.sharepoint.com/sites/EcoCommonsAustralia-ImplementationTeam/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FEcoCommonsAustralia%2DImplementationTeam%2FShared%20Documents%2FImplementation%20Team%2FEcoCommons%202020%2D2023%2FACTIVITY%20STREAMS%2FStream%203%20%2D%20Scientific%20and%20Data%20Workflows%2FPlatform%20Testing&viewid=50feca04%2Dca32%2D4c28%2Db55b%2D3a47aed2f3e8


### Setup example

#### Build the RStudio Dockerfile

```bash
docker build -f Dockerfile-rstudio -t rstudio .
```

#### Launch the container (example paths, exact local setup will vary)
This mounts a pipeline work dir previously created with `run_pipeline.sh` for interactive debugging.

```bash
# Start
RHOME=~/dev/
REPO=~/dev/sdm-function
docker run --rm -d --name rstudio -v $RHOME:/home/rstudio/ -v $REPO/nxf_work/:/workdir -p 127.0.0.1:8787:8787 -e DISABLE_AUTH=true rstudio

# Stop
docker stop rstudio
```

#### Open the RStudio web IDE 

http://127.0.0.1:8787/


### Debug a pipeline

This assumes `./bin/run_job.sh` or `./bin/run_pipeline.sh` has been run at least once and `./nxf_work` contains the working files.

- Open the RStudio web IDE.
- In *console* run `setwd('/workdir/script')`.
- Under *Files* navigate to `/workdir/script/` and open the integration R script.
- Select part or all of the script and run with Ctrl+Enter.
