# Workflow Functions - Deployment

Function deployment is managed via a Gitlab CI pipeline.

## Docker Images
Function Docker images are automatically built with a Gitlab pipeline following the standard branch/environment pattern adopted by all platform deployments.

COMING SOON