#!/usr/bin/env bash
# LOCAL DEV

# Example: ./bin/run_pipeline.sh bsspread.nf src/docs/examples/bsspread-example.json

set -e

[[ -z $1 || -z $2 ]] && printf "Usage: run_pipeline.sh PIPELINE_PATH PARAMS_PATH [JOB_ID]\n\n" && exit 1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
TIMESTAMP=$(date +%s)
PIPELINE=$1
PARAMS=$2
CPUS=${TASK_CPUS:-2}
JOB_ID=${3:-""}
AUTH_ENVFILE=${AUTH_ENVFILE:-"${SCRIPT_DIR}/../.env.auth"}
NXF_ENVFILE=${NXF_ENVFILE:-"${SCRIPT_DIR}/../.env"}
NXF_WORKDIR=${SCRIPT_DIR}/../nxf_work/
export NXF_ENVFILE

# Cleanup previous runs
rm -rf ${NXF_WORKDIR}/* ${SCRIPT_DIR}/../.nextflow.*.log

echo "using env: ${NXF_ENVFILE} ${AUTH_ENVFILE}"

# Create AUTH_ENVFILE if it does not exist
touch ${AUTH_ENVFILE} || true

# Run pipeline
nextflow run $PIPELINE \
    -w ${NXF_WORKDIR} \
    -profile ${NXF_PROFILE:-"local"} \
    -params-file $PARAMS \
    --absWorkdir '/workdir' \
    --publishDir $NXF_WORKDIR \
    --job_uuid $JOB_ID \
    --upload false \
    --inputs $PARAMS \
    --cpus $CPUS \
    --label 'smallmem' \
    --containerOptions "--env-file ${NXF_ENVFILE} --env-file ${AUTH_ENVFILE} -e JOB_WEBLOG_URL=${NXF_WEBLOG} --volume ${NXF_WORKDIR}:/workdir" \
    -name "job_${TIMESTAMP}" \
    -main-script $PIPELINE \
    -with-weblog ${NXF_WEBLOG} \
    -with-report "report.html" \
    -latest 1 

# nextflow log "job_${TIMESTAMP}" -f stdout,stderr