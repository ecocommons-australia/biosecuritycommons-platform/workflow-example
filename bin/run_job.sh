#!/usr/bin/env bash
# LOCAL DEV

# Requires: ./bin/envfile.txt or export NXF_ENVFILE
# Example: ./bin/run_job.sh ./bioclim.nf JOB_ID

set -e

[[ -z $1 || -z $2 ]] && printf "Usage: run_job.sh PIPELINE_LOCAL_PATH JOB_ID\n\n" && exit 1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
TIMESTAMP=$(date +%s)
PIPELINE=$1
PARAMS=${SCRIPT_DIR}/work/params.json
JOB_ID=$2
AUTH_ENVFILE=${AUTH_ENVFILE:-"${SCRIPT_DIR}/../.env.auth"}
NXF_WORKDIR=${SCRIPT_DIR}/../nxf_work/

# Cleanup previous runs
rm -rf ${SCRIPT_DIR}/work/* rm -rf ${NXF_WORKDIR}/*

# Create AUTH_ENVFILE if it does not exist
touch ${AUTH_ENVFILE} || true

# Dump params.json
docker-compose run --rm -e DEBUG=$DEBUG utils \
    /srv/app/manage.py runscript func_utils.scripts.support.dump_job_params --script-args \
    jobuuid=$JOB_ID

${SCRIPT_DIR}/run_pipeline.sh $PIPELINE $PARAMS $JOB_ID

# [[ $? != 0 ]] && nextflow log "job_${JOB_ID}_${TIMESTAMP}" -f stdout,stderr
