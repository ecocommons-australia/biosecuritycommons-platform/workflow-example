#!/usr/bin/env bash
set +x

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
DOCKER_IMAGE="registry.gitlab.com/ecocommons-australia/lib/django-func-utils/support:2.2.1"
OUTPUT_DIR=${SCRIPT_DIR}/../nxf_work/output/

docker run -v "${SCRIPT_DIR}/../tests/:/func_utils/tests/" \
		-v "${OUTPUT_DIR}:/output/" \
		-u $(id -u ${USER}):$(id -g ${USER}) \
		--rm $DOCKER_IMAGE bash \
		-c "compare_tiff.py $*"