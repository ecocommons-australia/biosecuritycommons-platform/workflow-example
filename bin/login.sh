#!/bin/bash
# Run with '. ./login.sh'

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

ENVFILE=${SCRIPT_DIR}/../.env
ENVFILE_AUTH=${SCRIPT_DIR}/../.env.auth

export KEYCLOAK_TOKEN=$($SCRIPT_DIR/venv/bin/python $SCRIPT_DIR/py/login.py $ENVFILE)

echo "KEYCLOAK_TOKEN=${KEYCLOAK_TOKEN}" > $ENVFILE_AUTH