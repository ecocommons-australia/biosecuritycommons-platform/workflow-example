#!/bin/bash
# Import Schema or Template to local API instance
# Usage: import_local.sh TYPE PATH VERSION REF
#    eg. import_local.sh schema src/doc/bsspread/ 0.0.1 develop

export CI_PROJECT_PATH="ecocommons-australia/biosecuritycommons-platform/bsc-functions"
export CI_COMMIT_SHA=$4
export KEYCLOAK_SERVER_URL=https://auth.dev.ecocommons.org.au/auth
export KEYCLOAK_REALM=ecocommons-dev
export KEYCLOAK_CLIENT_ID=function-execution
#export KEYCLOAK_CLIENT_SECRET_KEY= EXPORT ME BEFORE RUNNING SCRIPT
export WORKFLOW_MANAGER_API_TEMPLATE_URL="http://127.0.0.1:8000/workflow/template/"
export FUNCTION_MANAGER_API_SCHEMA_URL="http://127.0.0.1:8000/funcmanager/function/"

python3 -m pip install -qr ./bin/import/requirements.txt

python3 ./bin/import/import.py $1 $2 $3