# Questions and Answers

- Q: Where does the Python end and the R begin?
- A: Python serves as a wrapper for managing operatons specific to the platform lifecycle. Fetching raw data, creating metadata, talking to the platform APIs etc. Scientific Function specific calculations and data processing should ideally not be implemented in the Python wrapper. Conversely R code should only be concerned with science.  It should be unaware of where or how input data is fetched, and generally be designed with as little knowledge of the platform as possible.

- Q: Do I need to use the typical 3 step pipeline with `prepare/run/upload` steps?
- A: A Nextflow pipeline can be defined in any fashion and it will execute via the SLURM platform API. Additional pipeline processes can easily be added. However the `prepare` and `upload` processes are specifically designed for getting data in and out of the platform. Their behaviour operates on predefined pipeline and code repository conventions. 

- Q: Can I pull data directly from external sources when executing functions?
- A: There is a limited implementation for fetching arbitary raw data via the parameter interface `dataset_layers.source_type = 'external'`.  It has several limitations and is currently at a proof of concept state. Almost all data used in Function execution is retreived via platform API's via UUID.

