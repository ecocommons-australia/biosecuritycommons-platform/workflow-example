#!/usr/bin/env nextflow

params.function = null
params.upload = true

include { 
    createParamsJson;
    createInputdir;
    prepareInputs;
    runFunction;
    uploadResult;
} from './process'

include {
    runFunction as hello_world
} from './process'

/** 
 * Execute a standalone Job
 */
workflow job {
    take: input_params
    take: function
    take: job_uuid
    main:
        out = runFunction(
            prepareInputs(
                function, 
                job_uuid, 
                input_params, 
                createInputdir()
            )
        )

        if (params.upload == true){
            uploadResult(out)
        }
}

/**
 * Execute a JobRequestFunction
 */
workflow jobReqFunc {
    take: 
        jrf
        function
        inputdir

    main:
        createParamsJson(jrf)
        
        prepareInputs(
            createParamsJson.out,
            inputdir
        )

        process = function.replace('/', '_')
        out = "$process"(
            prepareInputs.out
        )

        if (params.upload == true){
            uploadResult(out)
        }

    emit:
        result  = out.results_zip
        jobinfo = out.jobinfo
        uuid    = createParamsJson.out.uuid
}