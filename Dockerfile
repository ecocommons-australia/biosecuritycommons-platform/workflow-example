ARG BASEIMG=registry.gitlab.com/ecocommons-australia/biosecuritycommons-platform/workflow-example/base
ARG BASEIMG_VERSION=latest

FROM ${BASEIMG}:${BASEIMG_VERSION}

# Install any R packages here

RUN mkdir -p /srv/app
WORKDIR /srv/app

COPY requirements.txt /srv/app
RUN pip install --no-cache-dir -r requirements.txt
COPY ./src /srv/app

ARG BUILD_ID
ENV BUILD_ID=$BUILD_ID